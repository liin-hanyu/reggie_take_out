package com.lhy.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {

}
