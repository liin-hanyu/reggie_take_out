package com.lhy.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
