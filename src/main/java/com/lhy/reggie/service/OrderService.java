package com.lhy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.entity.Orders;

public interface OrderService extends IService<Orders> {
    void submit(Orders orders);
}
