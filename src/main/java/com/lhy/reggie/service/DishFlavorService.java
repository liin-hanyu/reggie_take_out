package com.lhy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
