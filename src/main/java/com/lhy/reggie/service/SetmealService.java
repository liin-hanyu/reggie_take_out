package com.lhy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.dto.SetmealDto;
import com.lhy.reggie.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {
    /**
     * @Author LinHy
     * @Description 新增套餐，同时需要保存套餐和菜品的关联关系
     * @Date 22:12 2022/10/4
     * @Param [setmealDto]
     * @return void
     **/
    public void saveWithDish(SetmealDto setmealDto);
    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     * @param ids
     */
    public void removeWithDish(List<Long> ids);
}
