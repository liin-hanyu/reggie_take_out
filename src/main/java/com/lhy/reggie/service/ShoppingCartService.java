package com.lhy.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {

}
