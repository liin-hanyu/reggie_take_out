package com.lhy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.dto.DishDto;
import com.lhy.reggie.entity.Dish;

public interface DishService extends IService<Dish> {

    //新增菜品，同时插入菜品对应的口味数据，需要操作两张表，dish和dish_flavor
    public void saveWithFlavor(DishDto dishDto);

    //根据id查询菜品信息和对应的口味信息(用于修改菜品时回显信息使用，因为dish表中并没有flavors字段，所以不能直接查dish表，需要查dishDto表)
    public DishDto getByIdWithFlavor(Long id);


    //更新菜品信息同时更新口味信息
    public void updateWithFlavor(DishDto dishDto);
}
