package com.lhy.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.entity.User;

public interface UserService extends IService<User> {
}
