package com.lhy.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.reggie.dto.DishDto;
import com.lhy.reggie.entity.Dish;
import com.lhy.reggie.entity.DishFlavor;
import com.lhy.reggie.mapper.DishMapper;
import com.lhy.reggie.service.DishFlavorService;
import com.lhy.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  17:43
 * @Description: TODO
 * @Version: 1.0
 */
@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Autowired
    DishFlavorService dishFlavorService;

    @Override
   @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        //保存菜品的基本信息到菜品表dish
        this.save(dishDto);//this为DishServiceImpl

        //保存菜品口味数据到菜品口味表dish_flavor
            //由于前端传送的json数据中dish_flavor只有name和value，没有dishId，这会导致保存进数据库后菜品口味的dishId为空，这就没有意义了，我们就不知道这是哪个菜品的口味
            //所以需要获取dishDto.getId()菜品的dishId，再拿出dishDto中的dish_flavor列表进行遍历，把dishId加入dish_flavor表中赋值
        List<DishFlavor> flavors = dishDto.getFlavors();

        flavors = flavors.stream().map((item)->{
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());

        dishFlavorService.saveBatch(flavors);

    }


    /**
     * @Author LinHy
     * @Description 根据id查询菜品信息和对应的口味信息
     * @Date 10:39 2022/10/4
     * @Param [id]
     * @return com.lhy.reggie.dto.DishDto
     **/
    @Override
    public DishDto getByIdWithFlavor(Long id) {
        log.info("##################this的值为:{}",this);

        //1.查询菜品基本信息，从dish表查询
        Dish dish = this.getById(id);//this为DishServiceImpl

        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);

        //2.查询当前菜品对应的口味信息，从dish_flavor查询
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dish.getId());
        List<DishFlavor> list = dishFlavorService.list(queryWrapper);
        dishDto.setFlavors(list);

        return dishDto;
    }

    @Override
    public void updateWithFlavor(DishDto dishDto) {
        //1.更新菜品基本信息 更新dish表
        this.updateById(dishDto);


        //2.更新菜品口味信息 更新dish_flavor表
            //2.1清理当前菜品对应的口味数据 dish_flavor表的delete操作
            LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());
            dishFlavorService.remove(queryWrapper);

            //2.2插入新数据
            List<DishFlavor> flavors = dishDto.getFlavors();
                //由于当期的DishFlavor中dishId没有值(前端传送的json数据中dish_flavor只有name和value，没有dishId)，需要遍历当前的flavors，并从dishDto中获得当前菜品的dishId进行赋值
                flavors = flavors.stream().map((item)->{
                    item.setDishId(dishDto.getId());
                    return item;
                }).collect(Collectors.toList());
            dishFlavorService.saveBatch(flavors);

    }


}
