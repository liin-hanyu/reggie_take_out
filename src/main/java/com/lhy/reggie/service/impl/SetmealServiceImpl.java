package com.lhy.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.reggie.common.CustomException;
import com.lhy.reggie.dto.SetmealDto;
import com.lhy.reggie.entity.Setmeal;
import com.lhy.reggie.entity.SetmealDish;
import com.lhy.reggie.mapper.SetmealMapper;
import com.lhy.reggie.service.SetmealDishService;
import com.lhy.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  17:43
 * @Description: TODO
 * @Version: 1.0
 */
@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    SetmealDishService setmealDishService;

    /**
     * @Author LinHy
     * @Description
     * 新增套餐，同时需要保存套餐和菜品的关联关系
     *      前端传送的setmealDto中的setmealDishes里setmealid为null，原因是给套餐添加菜品的之后发送回来的数据只有菜品的数据，菜品数据里没有套餐id（setmealid），
     *      所以新增套餐分为两步：
     *          1.先把页面中套餐的基本信息加入setmeal表，可以直接用在SetmealServiceImpl中使用this.save(setmealDto)，因为setmealDto继承了setmeal，可以把页面中与setmeal对应的字段直接保存
     *           2.再将setmealid的值逐个放进每个setmealDishes中，再通过setmealDishService.saveBatch(setmealDishes)批量添加进setmeal_dish表
     * @Date 22:12 2022/10/4
     * @Param [setmealDto]
     * @return void
     **/
    @Override
    @Transactional
    public void saveWithDish(SetmealDto setmealDto) {
        //保存套餐基本信息,操作的是setmeal表 (但是setmealDishes里面只有dishid，没有setmealid)
        this.save(setmealDto);

        //获取setmealDishes列表，由于里面的setmealid为null，所以要遍历添加
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes.stream().map((item)->{
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        //保存套餐和菜品关联的信息，操作的是setmeal_dish表
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     * @param ids
     */
    @Transactional
    public void removeWithDish(List<Long> ids) {
        //select count(*) from setmeal where id in (1,2,3) and status = 1
        //查询套餐状态，确定是否可用删除
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.in(Setmeal::getId,ids);
        queryWrapper.eq(Setmeal::getStatus,1);

        int count = this.count(queryWrapper);
        if(count > 0){
            //如果不能删除，抛出一个业务异常
            throw new CustomException("套餐正在售卖中，不能删除");
        }

        //如果可以删除，先删除套餐表中的数据---setmeal
        this.removeByIds(ids);

        //delete from setmeal_dish where setmeal_id in (1,2,3)
        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId,ids);
        //删除关系表中的数据----setmeal_dish
        setmealDishService.remove(lambdaQueryWrapper);
    }
}
