package com.lhy.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.reggie.entity.Employee;
import com.lhy.reggie.mapper.EmployeeMapper;
import com.lhy.reggie.service.EmployeeService;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-02  21:09
 * @Description: TODO
 * @Version: 1.0
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

}
