package com.lhy.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.reggie.entity.DishFlavor;
import com.lhy.reggie.mapper.DishFlavorMapper;
import com.lhy.reggie.service.DishFlavorService;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  22:02
 * @Description: TODO
 * @Version: 1.0
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {

}
