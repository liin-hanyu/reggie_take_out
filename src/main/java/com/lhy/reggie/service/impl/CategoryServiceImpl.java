package com.lhy.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.reggie.common.CustomException;
import com.lhy.reggie.entity.Category;
import com.lhy.reggie.entity.Dish;
import com.lhy.reggie.entity.Setmeal;
import com.lhy.reggie.mapper.CategoryMapper;
import com.lhy.reggie.service.CategoryService;
import com.lhy.reggie.service.DishService;
import com.lhy.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  17:00
 * @Description: TODO
 * @Version: 1.0
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private DishService dishService;

    @Autowired
    private SetmealService setmealService;

    /**
     * @Author LinHy
     * @Description 根据id删除分类，删除前进行判断，若有菜品或套餐关联了这个分类，则无法删除
     * @Date 17:47 2022/10/3
     * @Param [id]
     * @return void
     **/
    @Override
    public void remove(Long id) {
        //1.查询当前分类是否关联菜品，若已经关联，抛出业务异常
                LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
                //添加查询条件
                dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);
                int count1 = dishService.count(dishLambdaQueryWrapper);
                if(count1>0){
                    //已经关联菜品，抛出异常
                    throw new CustomException("当前分类下已经关联了菜品，无法删除");
                }


        //2.查询当前分类是否关联套餐，若已经关联，抛出业务异常
                LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
                //添加查询条件
                setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId,id);
                int count2 = setmealService.count(setmealLambdaQueryWrapper);
                if(count2>0){
                    //已经关联套餐，抛出异常
                    throw new CustomException("当前分类下已经关联了套餐，无法删除");
                }

        //3.若都没有关联，则正常删除
        super.removeById(id);
    }
}
