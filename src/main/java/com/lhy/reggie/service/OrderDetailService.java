package com.lhy.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.reggie.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {

}
