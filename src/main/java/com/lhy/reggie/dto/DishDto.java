package com.lhy.reggie.dto;


import com.lhy.reggie.entity.Dish;
import com.lhy.reggie.entity.DishFlavor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * @Author LinHy
 * @Description  DTO全称data transfer object，即数据传输对象，一般用于展示层与服务层之间的数据传输
 * 使用该类的原因是因为：添加菜品时传输的参数中有flavors字段，而dish实体类中并没有该字段，这会导致服务端（dishController）获取前端传来的参数时无法将json数据绑定进实体类
 * @Date 22:37 2022/10/3
 * @Param
 * @return
 **/
@Data
public class DishDto extends Dish {

    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}
