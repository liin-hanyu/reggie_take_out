package com.lhy.reggie.dto;


import com.lhy.reggie.entity.Setmeal;
import com.lhy.reggie.entity.SetmealDish;
import lombok.Data;

import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
