package com.lhy.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lhy.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-02  21:08
 * @Description: TODO
 * @Version: 1.0
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

}
