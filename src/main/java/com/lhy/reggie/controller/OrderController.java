package com.lhy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lhy.reggie.common.BaseContext;
import com.lhy.reggie.common.R;
import com.lhy.reggie.entity.AddressBook;
import com.lhy.reggie.entity.Orders;
import com.lhy.reggie.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 用户下单
     * @param orders
     * 前端传送的Orders中只有addressBookId，payMethod，remark，因为用户已经登陆了，用户信息都已经存在了session中，包括购物车的数据也可以通过用户id得到
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){
        log.info("订单数据：{}",orders);
        orderService.submit(orders);
        return R.success("下单成功");
    }


    @GetMapping("/userPage")
    public R<List<Orders>> page(int page, int pageSize){
        Long userId = BaseContext.getCurrentId();

        //构造分页构造器
        Page<Orders> pageInfo = new Page<>(page,pageSize);

        //构造条件构造器
        LambdaQueryWrapper<Orders> lambdaQueryWrapper = new LambdaQueryWrapper();

        //添加条件(输入菜名搜索时name不为null，则满足下面的条件，进行筛选)
        lambdaQueryWrapper.like(Orders::getUserId,userId);
        //添加排序条件
        lambdaQueryWrapper.orderByDesc(Orders::getOrderTime);

        return R.success(orderService.list(lambdaQueryWrapper));
    }
}
