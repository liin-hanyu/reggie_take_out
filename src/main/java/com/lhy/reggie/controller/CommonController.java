package com.lhy.reggie.controller;

import com.lhy.reggie.common.R;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  21:29
 * @Description: 文件上传和下载
 * @Version: 1.0
 */

@RestController
@RequestMapping("/common")
public class CommonController {

    //application.yml配置文件中读取该属性
    @Value("${reggie.path}")
    private String basePath;


    /**
     * @Author LinHy
     * @Description 文件上传
     * @Date 21:50 2022/10/3
     * @Param [file]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PostMapping("/upload")
    public R<String> upload(MultipartFile file){
        //原始文件名
        String originalFilename = file.getOriginalFilename(); // abc.jpg
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));// .jpg

        //使用UUID重新生成文件名，防止文件名重复造成覆盖
        String filename = UUID.randomUUID().toString() + suffix;

        //创建一个目录对象
        File dir = new File(basePath);
        //判断当前目录是否存在
        if(!dir.exists()){
            //目录不存在，需要创建
            dir.mkdir();
        }

        try {
            file.transferTo(new File(basePath + filename));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return R.success(filename);
    }

    /**
     * @Author LinHy
     * @Description 文件下载
     * @Date 21:51 2022/10/3
     * @Param [name, response]
     * @return void
     **/
    @GetMapping("/download")
    public void download(String name, HttpServletResponse response){

        try {
            //输入流，通过输入流读取文件内容
            FileInputStream fileInputStream = new FileInputStream(new File(basePath + name));

            //输出流，通过输出流将文件写回浏览器
            ServletOutputStream outputStream = response.getOutputStream();

            response.setContentType("image/jpeg");

            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = fileInputStream.read(bytes)) != -1){
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }

            //关闭资源
            outputStream.close();
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
