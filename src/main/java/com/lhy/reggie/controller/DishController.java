package com.lhy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lhy.reggie.common.R;
import com.lhy.reggie.dto.DishDto;
import com.lhy.reggie.entity.Category;
import com.lhy.reggie.entity.Dish;
import com.lhy.reggie.entity.DishFlavor;
import com.lhy.reggie.service.CategoryService;
import com.lhy.reggie.service.DishFlavorService;
import com.lhy.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.quartz.QuartzTransactionManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  22:05
 * @Description: 菜品管理
 * @Version: 1.0
 */
@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;
    @Autowired
    private DishFlavorService dishFlavorService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * @Author LinHy
     * @Description 新增菜品
     * @Date 23:27 2022/10/3
     * @Param [dishDto]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto){
        log.info(dishDto.toString());
        dishService.saveWithFlavor(dishDto);

        //（优化：清理当前保存的菜品的缓存数据。）修改完菜品后需要把缓存中旧的菜品数据删除，因为页面加载时是加载缓存中的数据，若删除了缓存，缓存中没有了数据，就会在数据库中重新查找新的数据并加入缓存
        String key = "dish_" + dishDto.getCategoryId() + "_1";
        redisTemplate.delete(key);

        return  R.success("新增菜品成功");
    }


    /**
     * @Author LinHy
     * @Description 分页显示菜品
     * @Date 10:56 2022/10/4
     * @Param [page, pageSize, name]
     * @return com.lhy.reggie.common.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page>
     **/
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){

        //构造分页构造器
        Page<Dish> pageInfo = new Page<>(page,pageSize);
        Page<DishDto> dishDtoPage =new Page<>();

        //构造条件构造器
        LambdaQueryWrapper<Dish> lambdaQueryWrapper = new LambdaQueryWrapper();

        //添加条件(输入菜名搜索时name不为null，则满足下面的条件，进行筛选)
        lambdaQueryWrapper.like(name!=null,Dish::getName,name);
        //添加排序条件
        lambdaQueryWrapper.orderByDesc(Dish::getUpdateTime);

        //执行分页查询
        dishService.page(pageInfo,lambdaQueryWrapper);

        //对象拷贝
        BeanUtils.copyProperties(pageInfo,dishDtoPage,"records");

        List<Dish> records = pageInfo.getRecords();

        List<DishDto> list = records.stream().map((item) -> {
            DishDto dishDto = new DishDto();

            BeanUtils.copyProperties(item,dishDto);

            Long categoryId = item.getCategoryId();//分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            if(category != null){
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            return dishDto;
        }).collect(Collectors.toList());

        dishDtoPage.setRecords(list);

        return R.success(dishDtoPage);
    }

    /**
     * @Author LinHy
     * @Description 根据id查询菜品信息和对应的口味信息（用于展示在菜品管理页面中修改菜品时的菜品信息回显）
     * @Date 10:31 2022/10/4
     * @Param [id]
     * @return com.lhy.reggie.common.R<com.lhy.reggie.dto.DishDto>
     **/
    @GetMapping("/{id}")
    public R<DishDto> get(@PathVariable Long id){
        DishDto dishDto = dishService.getByIdWithFlavor(id);
        return R.success(dishDto);
    }


    /**
     * @Author LinHy
     * @Description 修改菜品
     * @Date 10:56 2022/10/4
     * @Param
     * @return
     **/
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto){
        dishService.updateWithFlavor(dishDto);

        //（优化：清理当前保存的菜品的缓存数据。）修改完菜品后需要把缓存中旧的菜品数据删除，因为页面加载时是加载缓存中的数据，若删除了缓存，缓存中没有了数据，就会在数据库中重新查找新的数据并加入缓存
        String key = "dish_" + dishDto.getCategoryId() + "_1";
        redisTemplate.delete(key);

        return R.success("修改菜品成功");
    }


    /**
     * @Author LinHy
     * @Description 根据条件(菜品分类id:CategoryId)查询菜品信息（1.用于套餐管理中添加菜品时展示菜品信息 2.用于客户端首页展示菜品）
     *
     * @Date 21:52 2022/10/4
     * @Param [dish]
     * @return com.lhy.reggie.common.R<java.util.List<com.lhy.reggie.entity.Dish>>
     **/
    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish){
        List<DishDto> dishDtoList = null;
                //（优化：）
            //动态构造key
            String key = "dish_" + dish.getCategoryId() + "_" + dish.getStatus();//dish_1397844391040167938_1

            //先从redis中获取缓存数据
            dishDtoList = (List<DishDto>) redisTemplate.opsForValue().get(key);

            if(dishDtoList!=null){
                //如果存在，直接返回，无需找数据库
                return R.success(dishDtoList);
            }

        //构造查询条件
        LambdaQueryWrapper<Dish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(dish.getCategoryId()!=null,Dish::getCategoryId,dish.getCategoryId());
        //添加条件，查询状态为1（起售状态）的菜品
        lambdaQueryWrapper.eq(Dish::getStatus,1);
        //添加排序条件
        lambdaQueryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);

        List<Dish> list = dishService.list(lambdaQueryWrapper);

        dishDtoList = list.stream().map((item) -> {
            DishDto dishDto = new DishDto();

            BeanUtils.copyProperties(item,dishDto);

            Long categoryId = item.getCategoryId();//分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            if(category != null){
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }

            //当前菜品的id
            Long dishId = item.getId();
            LambdaQueryWrapper<DishFlavor> QueryWrapper = new LambdaQueryWrapper<>();
            QueryWrapper.eq(DishFlavor::getDishId,dishId);
            //SQL:select * from dish_flavor where dish_id = ?
            List<DishFlavor> dishFlavorList = dishFlavorService.list(QueryWrapper);
            dishDto.setFlavors(dishFlavorList);
            return dishDto;
        }).collect(Collectors.toList());

        //如果不存在，需要查询数据库，将查询到的数据缓存到redis
        redisTemplate.opsForValue().set(key,dishDtoList,60, TimeUnit.MINUTES);
        return R.success(dishDtoList);
    }



}
