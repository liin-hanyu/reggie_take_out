package com.lhy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lhy.reggie.common.R;
import com.lhy.reggie.dto.DishDto;
import com.lhy.reggie.dto.SetmealDto;
import com.lhy.reggie.entity.Category;
import com.lhy.reggie.entity.Dish;
import com.lhy.reggie.entity.Setmeal;
import com.lhy.reggie.service.CategoryService;
import com.lhy.reggie.service.DishService;
import com.lhy.reggie.service.SetmealDishService;
import com.lhy.reggie.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-04  21:39
 * @Description: TODO
 * @Version: 1.0
 */

@RestController
@RequestMapping("/setmeal")

public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    CategoryService categoryService;

    /**
     * @Author LinHy
     * @Description 新增套餐（具体参考）

     * @Date 22:40 2022/10/4
     * @Param [setmealDto]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PostMapping
    @CacheEvict(value = "setmealCache",allEntries = true) //allEntries = true表示删除该value（setmealCache）下的所有缓存，默认是false
    public R<String> save(@RequestBody SetmealDto setmealDto){
        setmealService.saveWithDish(setmealDto);

        return R.success("新增套餐成功");
    }

    /**
     * @Author LinHy
     * @Description
     * 套餐分页查询
     *      最后查出来的pageinfo中没有包含页面所需要的套餐分类信息（categoryName），因为setmeal表中就没有categoryName这个字段，
     *      所以需要再使用一次SetmealDto表,使用对象拷贝（66行）,在把categoryName查出来设置进SetmealDto中，最后返回dtoPage
     *
     * @Date 22:52 2022/10/4
     * @Param [page, pageSize, name]
     * @return com.lhy.reggie.common.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page>
     **/
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name) {
        Page<Setmeal> pageinfo = new Page<>(page, pageSize);
        Page<SetmealDto> dtoPage = new Page<>();

        LambdaQueryWrapper<Setmeal> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.like(name != null, Setmeal::getName, name);
        lambdaQueryWrapper.orderByDesc(Setmeal::getUpdateTime);
        setmealService.page(pageinfo, lambdaQueryWrapper);

        //对象拷贝
        BeanUtils.copyProperties(pageinfo, dtoPage, "records");
        List<Setmeal> records = pageinfo.getRecords();

        List<SetmealDto> list = records.stream().map((item) -> {
            SetmealDto setmealDto = new SetmealDto();
            //对象拷贝
            BeanUtils.copyProperties(item, setmealDto);
            //分类id
            Long categoryId = item.getCategoryId();
            //根据分类id查询分类对象
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                //分类名称
                String categoryName = category.getName();
                setmealDto.setCategoryName(categoryName);
            }
            return setmealDto;
        }).collect(Collectors.toList());

        dtoPage.setRecords(list);
        return R.success(dtoPage);
    }
    /**
     * 删除套餐
     * @param ids
     * @return
     */
    @DeleteMapping
    @CacheEvict(value = "setmealCache",allEntries = true) //allEntries = true表示删除该value（setmealCache）下的所有缓存，默认是false
    public R<String> delete(@RequestParam List<Long> ids){

        setmealService.removeWithDish(ids);

        return R.success("套餐数据删除成功");
    }

    /**
     * 根据条件查询套餐数据
     * @param setmeal
     * @return
     */
    @GetMapping("/list")
    @Cacheable(value = "setmealCache",key = "#setmeal.categoryId + '_' + #setmeal.status")
    public R<List<Setmeal>> list(Setmeal setmeal){
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null,Setmeal::getCategoryId,setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null,Setmeal::getStatus,setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        List<Setmeal> list = setmealService.list(queryWrapper);

        return R.success(list);
    }

    @GetMapping("/dish/{id}")
    @Cacheable(value = "setmealCache",key = " 'setmeal_'+ #id")
    public R<Setmeal> show(@PathVariable Long id){
        Setmeal setmeal = setmealService.getById(id);
        return R.success(setmeal);
    }


}
