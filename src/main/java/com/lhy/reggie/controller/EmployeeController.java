package com.lhy.reggie.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lhy.reggie.common.R;
import com.lhy.reggie.entity.Employee;
import com.lhy.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-02  21:08
 * @Description: TODO
 * @Version: 1.0
 */
@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * @Author LinHy
     * @Description 员工登入
         * 1.密码md5加密
         * 2.根据用户名查询数据库
         * 3.密码比对是否一致
         * 4.查看员工是否被禁用
         * 5.将员工id放入session，返回成功结果
     *
     * @Date 21:26 2022/10/2
     * @Param [request, employee]
     * @return com.lhy.reggie.common.R<com.lhy.reggie.entity.Employee>
     **/
    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee){
        //1.密码md5加密
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        // 2.根据用户名查询数据库
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        //queryWrapper.eq()表示等值查询，对应到sql语句就是where xxx = “xxx”  ，Employee::getUsername是lambda表达式，表示new一个Employee，调用其中的方法getUsername
        queryWrapper.eq(Employee::getUsername,employee.getUsername());
        Employee emp = employeeService.getOne(queryWrapper);
            //2.1 查询失败，返回失败结果
            if(emp == null){
                return R.error("登入失败");
            }

        //3.密码比对是否一致
            //3.1 密码错误，返回失败结果
            if(!emp.getPassword().equals(password)){
                return R.error("登入失败");
            }

        //4.查看员工是否被禁用
            if(emp.getStatus() == 0){
                return R.error("登入失败,账号已禁用");
            }


        //5.将员工id放入session，返回成功结果
        request.getSession().setAttribute("employee",emp.getId());
        return R.success(emp);
    }


    /**
     * @Author LinHy
     * @Description 员工退出登入
     * @Date 13:19 2022/10/3
     * @Param [request]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PostMapping("logout")
    public R<String> logout(HttpServletRequest request){
        //清理Session中保存的当前登入的员工的id
        request.getSession().removeAttribute("employee");
        return  R.success("退出成功");
    }


    /**
     * @Author LinHy
     * @Description
     * 添加员工
     * @Date 13:18 2022/10/3
     * @Param [employee, request]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PostMapping
    public R<String> sava(@RequestBody Employee employee,HttpServletRequest request){
        log.info("新增员工信息为：{}",employee.toString());

// 注释掉的原因是，以下字段属于公共字段，在修改，添加等方法中都会用到，所以提取出来在MyMetaObjecthandler类中统一处理
//        //设置初始值
//            //初始密码为123456
//            employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes(StandardCharsets.UTF_8)));
//            employee.setCreateTime(LocalDateTime.now());
//            employee.setUpdateTime(LocalDateTime.now());
//            Long empId = (Long) request.getSession().getAttribute("employee");
//            employee.setCreateUser(empId);
//            employee.setUpdateUser(empId);

        //新增员工
        employeeService.save(employee);

        return R.success("添加新员工成功");
    }


    /**
     * @Author LinHy
     * @Description 首页分页显示员工数据
     * @Date 13:18 2022/10/3
     * @Param [page, pageSize, name]
     * @return com.lhy.reggie.common.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page>
     **/
    @GetMapping("page")
    public R<Page> page(int page, int pageSize, String name){ //int page前可以不写@RequestParam(value="page", required=false) 注解，原因是只要参数名与请求中的参数对应，就可以省略，如果不对应，则需要value="page"设置请求中的参数名
        //1.构造分页构造器
        Page pageInfo = new Page(page,pageSize);

        //2.构造条件构造器
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
            //2.1添加条件构造器（输入姓名查询）
            queryWrapper.like(StringUtils.isNotEmpty(name),Employee::getName,name);
            //2.2添加排序条件
            queryWrapper.orderByDesc(Employee::getUpdateTime);

        //3.执行查询
        employeeService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);
    }


    /**
     * @Author LinHy
     * @Description 根据id修改员工信息
     * @Date 13:18 2022/10/3
     * @Param [employee, request]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PutMapping
    public R<String> update(@RequestBody Employee employee,HttpServletRequest request){
        //1.获取修改人id，设置更新时间，设置修改人id
        Long empId = (Long) request.getSession().getAttribute("employee");
        employee.setUpdateTime(LocalDateTime.now());
        employee.setUpdateUser(empId);

        employeeService.updateById(employee);

        return R.success("员工信息修改成功");
    }


    /**
     * @Author LinHy
     * @Description 根据id查询员工信息（可用于点击编辑时，回显被点击员工的信息在页面上）
     * @Date 13:19 2022/10/3
     * @Param [id]
     * @return com.lhy.reggie.common.R<com.lhy.reggie.entity.Employee>
     **/
    @GetMapping("/{id}")
    public R<Employee> getById(@PathVariable Long id){
        Employee emp = employeeService.getById(id);
        return R.success(emp);
    }

}
