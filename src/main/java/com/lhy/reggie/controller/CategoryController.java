package com.lhy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lhy.reggie.common.R;
import com.lhy.reggie.entity.Category;
import com.lhy.reggie.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  17:02
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * @Author LinHy
     * @Description 新增菜品分类
     * @Date 17:08 2022/10/3
     * @Param [category]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PostMapping
    public R<String> sava(@RequestBody Category category){
        categoryService.save(category);
        return R.success("新增分类成功");
    }

    /**
     * @Author LinHy
     * @Description 分类显示（分类管理页）
     * @Date 17:31 2022/10/3
     * @Param [page, pageSize]
     * @return com.lhy.reggie.common.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page>
     **/
    @GetMapping("/page")
    public R<Page> page (int page,int pageSize){
        //1.分页构造器
        Page pageInfo = new Page(page,pageSize);

        //2.条件构造器
        LambdaQueryWrapper<Category> lambdaQueryWrapper =  new LambdaQueryWrapper<>();
            //添加排序条件，根据sort进行排序
            lambdaQueryWrapper.orderByAsc(Category::getSort);

        //3.分页查询
        categoryService.page(pageInfo,lambdaQueryWrapper);
        return R.success(pageInfo);
    }

    /**
     * @Author LinHy
     * @Description 根据id删除分类（分类管理页）
     * @Date 17:37 2022/10/3
     * @Param [id]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @DeleteMapping
    public R<String> delete(Long id){
        categoryService.remove(id);
        return R.success("分类信息删除成功!");
    }

    /**
     * @Author LinHy
     * @Description 修改分类信息(分类管理页)
     * @Date 18:30 2022/10/3
     * @Param [category]    前端传送的参数为id,name,sort,参考\category\list.html的273行editCategory({'id':this.classData.id,'name': this.classData.name, sort: this.classData.sort})
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @PutMapping
    public R<String> update(@RequestBody Category category){ //由于Category实体类中公共字段标注了@TableField注解，所以会自动赋值，而剩下的属性id，name，sort由@RequestBody注解接收前端参数后自动配置

        categoryService.updateById(category);
        return R.success("修改分类信息成功");
    }


    /**
     * @Author LinHy
     * @Description
     * 获取菜品分类列表（用于添加菜品时的菜品分类下拉框）
     * @Date 22:28 2022/10/3
     * @Param [category]
     * @return com.lhy.reggie.common.R<java.util.List<com.lhy.reggie.entity.Category>>
     **/
    @GetMapping("list")
    public R<List<Category>> list(Category category){
        //条件构造器
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        //添加条件
        lambdaQueryWrapper.eq(category.getType()!=null,Category::getType,category.getType());

        //添加排序条件
        lambdaQueryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);

        List<Category> list = categoryService.list(lambdaQueryWrapper);
        return R.success(list);
    }





}
