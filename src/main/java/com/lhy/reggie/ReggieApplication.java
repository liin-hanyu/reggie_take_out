package com.lhy.reggie;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-02  17:53
 * @Description: TODO
 * @Version: 1.0
 */
@Slf4j
@SpringBootApplication
@ServletComponentScan //加了这个注解，启动时会扫描webfilter注解，加载过滤器
@EnableTransactionManagement //开启事务注解的支持
@EnableCaching //开启spring cache 注解方式的缓存功能
public class ReggieApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReggieApplication.class,args);
        log.info("成功");
    }
}
