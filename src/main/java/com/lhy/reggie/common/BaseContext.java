package com.lhy.reggie.common;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  14:13
 * @Description: 基于ThreadLocal封装的工具类，用户保存和获取当前登录用户的id，并在MyMetaObjecthandler类中被调用
 * @Version: 1.0
 */
public class BaseContext {
    //ThreadLocal作用范围是同一个线程之内，客户端每发一次http请求服务器都会给他一个线程
        // 以修改为例：LoginCheckFilter中的doFilter，EmployeeController中的update以及MyMetaObjecthandler中的updateFill方法都在同一个线程内
        // 所以只要在LoginCheckFilter类中的doFilter中获取到id并加入ThreadLocal中，该线程内的方法就都可以获得ThreadLocal中保存的字段,这样MyMetaObjecthandler类中设置修改信息时就可以获取到修改人的id
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }

    public static Long getCurrentId(){
        return threadLocal.get();
    }


}
