package com.lhy.reggie.common;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  18:00
 * @Description: 自定义业务异常类
 * @Version: 1.0
 */
public class CustomException extends RuntimeException{
    public CustomException(String msg){
        super(msg);
    }
}
