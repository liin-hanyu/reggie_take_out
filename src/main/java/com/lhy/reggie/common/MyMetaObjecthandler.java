package com.lhy.reggie.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  13:46
 * @Description: 自定义元数据对象处理器(主要目的是给实体类对象自动填充某些属性，例如employee实体类的创建时间，修改时间，创建人等属性，避免代码冗余)
 * @Version: 1.0
 */

@Component
public class MyMetaObjecthandler implements MetaObjectHandler {

    /**
     * @Author LinHy
     * @Description 插入操作时自动填充公共字段(EmployeeController类108行注释的内容)
     * @Date 13:57 2022/10/3
     * @Param [metaObject]
     * @return void
     **/
    @Override
    public void insertFill(MetaObject metaObject) {
        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime", LocalDateTime.now());
        metaObject.setValue("createUser", BaseContext.getCurrentId()); //BaseContext.getCurrentId()的具体解释参考BaseContext类
        metaObject.setValue("updateUser", BaseContext.getCurrentId());
    }

    /**
     * @Author LinHy
     * @Description 修改操作时自动填充公共字段
     * @Date 13:57 2022/10/3
     * @Param [metaObject]
     * @return void
     **/
    @Override
    public void updateFill(MetaObject metaObject) {
        metaObject.setValue("updateTime", LocalDateTime.now());
        metaObject.setValue("updateUser", BaseContext.getCurrentId());
    }
}
