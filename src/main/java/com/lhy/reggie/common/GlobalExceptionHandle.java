package com.lhy.reggie.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @BelongsProject: reggie_take_out
 * @Author: LinHy
 * @CreateTime: 2022-10-03  00:01
 * @Description: 全局异常处理，一旦抛出异常，统一在此类处理
 * @Version: 1.0
 */

//该注解表示 标有RestController.class和Controller.class注解的类都会被此类处理
@ControllerAdvice(annotations = {RestController.class, Controller.class})
@ResponseBody
@Slf4j
public class GlobalExceptionHandle {

    /**
     * @Author LinHy
     * @Description 异常处理方法
     * @Date 0:05 2022/10/3
     * @Param []
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R<String> exceptionHandle(SQLIntegrityConstraintViolationException ex){
        log.error(ex.getMessage());

        //判断异常信息中是否包含"Duplicate entry"（数据库设计时，设置了账号唯一，若账号重复注册，则触发账号已存在的异常，异常信息会包含"Duplicate entry"）
        if(ex.getMessage().contains("Duplicate entry")){
            String[] split = ex.getMessage().split(" ");
            String msg = split[2] + "已存在";
            return R.error(msg);
        }
        return R.error("未知错误");
    }

    /**
     * @Author LinHy
     * @Description 处理自定义业务异常
     * @Date 18:04 2022/10/3
     * @Param [ex]
     * @return com.lhy.reggie.common.R<java.lang.String>
     **/
    @ExceptionHandler(CustomException.class)
    public R<String> exceptionHandle(CustomException ex){
        log.error(ex.getMessage());
        return R.error(ex.getMessage());
    }



}
