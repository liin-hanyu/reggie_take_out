package com.lhy.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 员工实体
 */
@Data
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String username;

    private String name;

    private String password;

    private String phone;

    private String sex;

    private String idNumber; //身份证号码

    private Integer status;


    //该注解是指定哪些字段是公共字段
    @TableField(fill = FieldFill.INSERT)  //插入时自动填充字段，避免代码冗余,fill = FieldFill.INSERT是处理策略，当insert时自动填充这个字段值，具体实现在com.lhy.reggie.common.MyMetaObjecthandler类中
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE) //插入时自动填充字段
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT) //插入时自动填充字段
    private Long createUser;

    @TableField(fill = FieldFill.INSERT_UPDATE) //插入时自动填充字段
    private Long updateUser;

}
